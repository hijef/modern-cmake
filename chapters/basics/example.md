# A simple example

This is a simple yet complete example of a proper CMakeLists. For this program, we have one library (MyLibExample) with a header file and a source file,
and one application, MyExample,  with one source file. 

2[import:'main', lang:'cmake'](../../examples/simple-project/CMakeLists.txt)

The complete example is available in [examples folder](https://gitlab.com/CLIUtils/modern-cmake/tree/master/examples/simple-project).
